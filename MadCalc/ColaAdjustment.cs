﻿using System.Collections.Generic;

namespace MadCalc
{
    public class ColaAdjustment : Calculation
    {
        public ColaAdjustment()
            : base("C3300 COLA Adjustment")
        {
        }

        protected override List<Cell> InitialState
        {
            get
            {
                return new List<Cell>
                {
                    Input("B7", "COLA", "=DATE(2014,4,23)", Cell.CellType.DateTime),
                    Input("B9", "DoQ", "=DATE(1988,5,11)", Cell.CellType.DateTime),
                    Input("B11", "BasicAllow", "7622.15"),
                    Input("E11", "FirstCOLA", "=DATE(1989,4,27)", Cell.CellType.DateTime),
                    Input("B15", "2008COLA", "95"),
                    Input("E15", "LastCOLA", "=DATE(2013,4,1)", Cell.CellType.DateTime),
                    Input("B36", "CPITo2007", "66.95"),
                    Input("E44", "Increase", "2.4"),
                    Input("B29", "AdjCPIPre2009", "15.43"),
                    Input("B27", "IncreaseSinceDOQ", "92.71"),

                    Intermediate("B19", "DoQMin", "=MAX(DATE(1956,3,31),B9)", Cell.CellType.DateTime),
                    Intermediate("F19", "FirstAdj", "=IF(MONTH(B19)>=10, DATE(YEAR(B19)+2, 4,1), DATE(YEAR(B19)+1, 4,1))", Cell.CellType.DateTime),
                    Intermediate("F21", "COLABase", "=INT((DATE(YEAR(F19),MONTH(F19),28)-DATE(1956,4,12))/28)*28+DATE(1956,4,12)", Cell.CellType.DateTime),
                    Intermediate("B25", "Qual", "=IF(AND(DATE(YEAR(B9),MONTH(B9), DAY(B9)) >= DATE(YEAR(B9),4,1),DATE(YEAR(B9),MONTH(B9), DAY(B9)) <= DATE(YEAR(B9),9,30)),\"1 Apr to 30 Sept\", \"1 Oct to 31 Mar\")"),
                    Intermediate("B37", "COLAto2008", "=ROUNDUP(B11*B15/100*B36/100,2)"),
                    Intermediate("B38", "AdjPre2009", "=ROUND(B11+B37,2)"),
                    Intermediate("B45","AdjCPIPre2009", "=ROUND(B29/100,4)"),
                    Intermediate("B46", "AddCOLAPre2009", "=ROUNDUP(B45*B38,2)"),
                    Intermediate("B47", "AllowPre2009", "=ROUND(IF(B15>0,B38+B46,0),2)"),
                    Intermediate("B50", "IncreaseSinceDOQ", "=ROUND(B27/100,4)"),
                    Intermediate("B51", "AddCOLA", "=IF(E11>B7, 0, ROUNDUP(B50*B11,2))"),
                    Intermediate("B52", "AllowSubjectToCOLA", "=ROUND(IF(B15>0,0,B51+B11),2)"),
  
                    Output("B57", "COLAIncrease", "=IF(AND(B15>0,B15<100),B46+B37,B51)"), 
                    Output("B59", "CurrentAllow", "=ROUND(IF(E11>B7,B11,B47+B52),2)"),
                    Output("E59", "LastCOLAIncDate", "=IF(OR(E44<0.5, E11>B7),IF(E15=\"\", \"\", E15),B7)", Cell.CellType.DateTime)
                };
            }
        }

    }
}
