﻿
namespace MadCalc
{
    public class Cell
    {
        public Cell(string gridref, string label, string value, CellType type, CalcStage calcStage)
        {
            GridRef = gridref;
            Label = label;
            Value = value;
            Type = type;
            Stage = calcStage;
        }

        public enum CalcStage
        {
            Input,
            Intermediate,
            Output
        }

        public enum CellType
        {
            Number,
            DateTime
        }

        public string GridRef { get; set; }

        public string Label { get; set; }

        public string Value { get; set; }

        public CalcStage Stage { get; set; }

        public CellType Type { get; set; }
    }
}
