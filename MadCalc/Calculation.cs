﻿using System;
using System.Collections.Generic;
using System.Linq;
using Spire.Xls;

namespace MadCalc
{
    public abstract class Calculation
    {
        private readonly string _name;
        private readonly Workbook _workbook;
        private readonly Worksheet _worksheet;

        protected Calculation(string name)
        {
            Console.WriteLine("\nPerforming Calculation: {0}", name);
            Console.WriteLine("===============================================", name);

            _name = name;
            _workbook = new Workbook();
            _worksheet = _workbook.Worksheets[0];
            _worksheet.Name = _name;
            _workbook.CalculationMode = ExcelCalculationMode.Auto;
        }

        protected abstract List<Cell> InitialState { get; }

        public void Evaluate()
        {
            SetInitialState();

            _workbook.CalculateAllValue();

            var dump = _workbook.ActiveSheet.Cells.Where(cell => !string.IsNullOrWhiteSpace(cell.Comment.Text))
                .Select(cell => string.Format("{0} => {1}", cell.Comment.Text, cell.FormulaValue ?? cell.Value))
                .OrderBy(c => c);

            foreach (var line in dump)
            {
                Console.WriteLine(line);
            }
        }

        protected Cell Input(string gridRef, string label, string value)
        {
            return new Cell(gridRef, label, value, Cell.CellType.Number, Cell.CalcStage.Input);
        }

        protected Cell Input(string gridRef, string label, string value, Cell.CellType type)
        {
            return new Cell(gridRef, label, value, type, Cell.CalcStage.Input);
        }

        protected Cell Intermediate(string gridRef, string label, string value)
        {
            return new Cell(gridRef, label, value, Cell.CellType.Number, Cell.CalcStage.Intermediate);
        }

        protected Cell Intermediate(string gridRef, string label, string value, Cell.CellType type)
        {
            return new Cell(gridRef, label, value, type, Cell.CalcStage.Intermediate);
        }

        protected Cell Output(string gridRef, string label, string value, Cell.CellType type)
        {
            return new Cell(gridRef, label, value, type, Cell.CalcStage.Output);
        }

        protected Cell Output(string gridRef, string label, string value)
        {
            return new Cell(gridRef, label, value, Cell.CellType.Number, Cell.CalcStage.Output);
        }

        private void SetInitialState()
        {
            foreach (var cell in InitialState)
            {
                if (cell.Type == Cell.CellType.DateTime)
                {
                    if (cell.Value.StartsWith("="))
                        _worksheet.Range[cell.GridRef].Formula = cell.Value;
                    else
                        _worksheet.Range[cell.GridRef].DateTimeValue = Convert.ToDateTime(cell.Value);

                    _worksheet.Range[cell.GridRef].NumberFormat = "yyyy/mm/dd";
                }
                else if (cell.Type == Cell.CellType.Number)
                {
                    if (cell.Value.StartsWith("="))
                    {
                        _worksheet.Range[cell.GridRef].Formula = cell.Value;
                    }
                    else
                    {
                        _worksheet.Range[cell.GridRef].Text = cell.Value;
                    }
                }

                _worksheet.Range[cell.GridRef].AddComment().Text = String.Format("{0,-13}:{1,-4}:{2,-16}", cell.Stage, cell.GridRef, cell.Label);
            }
        }


    }
}
