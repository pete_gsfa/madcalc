﻿using System.Collections.Generic;

namespace MadCalc
{
    public class ColaDateOfQualification : Calculation
    {
        public ColaDateOfQualification() 
            : base("C3300 Date of Qualification")
        {
        }

        protected override List<Cell> InitialState
        {
            get
            {
                return new List<Cell>
                {
                    Input("B6", "Scenario",  "Death of Member"),
                    Input("B8", "EffectiveDate",  "=DATE(2015, 08, 1)", Cell.CellType.DateTime),
                    Input("B13", "OriginalDoQ", "=DATE(2011, 08, 1)", Cell.CellType.DateTime),
                    Input("E13", "FirstColaDate", "=DATE(2012, 05, 1)", Cell.CellType.DateTime),

                    Intermediate("C18", "DeathNewAnnuity", "=IF(LEFT(B6,5)=\"Death\",\"Y\",\"N\")"),
                    Intermediate("C20", "DeathBeforeRetire", "=IF(AND(B13<=0,C18=\"Y\"),B8,\" \")"),
                    Intermediate("C21", "DeathAfterRetire", "=IF(AND(C18=\"Y\",B8>B13,B8<E13),B13,IF(AND(B13>0,C18=\"y\"),E21,\" \"))", Cell.CellType.DateTime),
                    Intermediate("C22", "NewAllowances", "=IF(C18=\"N\",B8,\" \")", Cell.CellType.DateTime),
                    Intermediate("E20", "AprilFinYr", "=IF(B8>DATE(YEAR(B8),3,31),DATE(YEAR(B8),4,1),DATE(YEAR(B8)-1,4,1))", Cell.CellType.DateTime),
                    Intermediate("E21", "Int1", "=IF(AND(YEAR(B8)=YEAR(E25),MONTH(B8)=4,B8<E25),DATE(YEAR(E20)-1,MONTH(E20),DAY(E20)),E20)", Cell.CellType.DateTime), 
                    Intermediate("E25", "Int2", "=INT((DATE(YEAR(B8),4,28)-DATE(1956,4,12))/28)*28+DATE(1956,4,12)", Cell.CellType.DateTime),
                    Intermediate("C27", "FirstAdjDate", "=IF(MONTH(C25)>=10, DATE(YEAR(C25)+2, 4,1), DATE(YEAR(C25)+1, 4,1))", Cell.CellType.DateTime),
                    Intermediate("E28", "FirstAdjDate	IntFirstCOLA Date", "=INT((DATE(YEAR(C27),MONTH(C27),28)-DATE(1956,4,12))/28)*28+DATE(1956,4,12)", Cell.CellType.DateTime),
                    Intermediate("C25", "QualifyDate", "=MAX(C20:C22)", Cell.CellType.DateTime),

                    Output("B32", "DoQ", "=C25", Cell.CellType.DateTime),
                    Output("E32", "FirstCOLADate", "=E28", Cell.CellType.DateTime)
                };
            }
        }
    }
}
