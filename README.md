# MadCalc #

### What is MadCalc ###
Its a proof-of-concept to speed up the migration of calculations from Excel spreadsheets into PAMS green.

This is a simple console app for now to prove the point. The following calcs have been implemented as a test:
 * C3300 Date of Qualification
 * C3300 Cola Adjustment

### Benefits ###
 * FAST to implement (COLA Adjust took 10 mins vs 3 1/2 days for C# port)
 * Natural-order evaluation (ordering doesn't matter)
 * No marshalling/handling/assigning variables - much more Functional/Declaritive.
 * Should be much less chances for bugs.

### Concerns ###
 * Slow - def not as fast as native C# 0.28 secs for COLA Adjust calc on average. Too slow for running many 10s of thousands for an annual COLA run
 * Different langauge? - You have to have some familiarity with Excel functions etc. Harder to maintain? I dont tend to think so.
 * some very minor tweaks to excel formulas

### What do i want from this? ###

* Review the concept
* Feedback please
* Consideration of how we may implement this for real - if at all?

### Who do I talk to? ###

* Pete G.